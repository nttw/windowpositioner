package org.bitbucket.nttw.windowpositioner;

import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.Structure;
import com.sun.jna.win32.StdCallLibrary;

public interface User32 extends StdCallLibrary  {
	User32 INSTANCE = (User32) Native.loadLibrary("user32", User32.class);
	User32 SYNC_INSTANCE = (User32) Native.synchronizedLibrary(INSTANCE);

	public static class Rect extends Structure {
		public int x;
		public int y;
		public int s;
		public int t;

		@Override
		public String toString() {
			return x + " " + y + " " + s + " " + t;
		}
	}

	// Callback replaced with StdCallBack
	public static interface WndEnumProc extends StdCallCallback {
		boolean wndEnumProcCallback(Pointer hWnd, int lParam);
	}

	// out Rect
	public boolean GetWindowRect(Pointer hWnd, Rect lpRect);
	public int EnumWindows(WndEnumProc callPtr, int lPar);
	public boolean MoveWindow(Pointer hWnd, int x, int y, int nWidth, int nHeight, boolean bRepaint);
	public int IsWindowVisible(Pointer hWnd);

	public int GetWindowTextA(Pointer hWnd, byte[] lpString, int nMaxCount);
	public int GetWindowTextLengthA(Pointer hWnd);
}
