package org.bitbucket.nttw.windowpositioner;

import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JList;
import org.bitbucket.nttw.windowpositioner.User32.Rect;

public class WPFrame extends javax.swing.JFrame {
	private static final long serialVersionUID = 1L;
	private WPFacility wp;

    /** Creates new form WPFrame */
    public WPFrame() {
        initComponents();
		init();
    }

    /** This method is called from within the constructor to
     * initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is
     * always regenerated by the Form Editor.
     */
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jList = new javax.swing.JList();
        jTextFieldX = new javax.swing.JTextField();
        jTextFieldY = new javax.swing.JTextField();
        jTextFieldW = new javax.swing.JTextField();
        jTextFieldH = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        jButton3 = new javax.swing.JButton();
        jButton4 = new javax.swing.JButton();
        jButtonC = new javax.swing.JButton();
        jButtonU = new javax.swing.JButton();
        jButtonR = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("WindowPositioner4");

        jList.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jList.addListSelectionListener(new javax.swing.event.ListSelectionListener() {
            public void valueChanged(javax.swing.event.ListSelectionEvent evt) {
                jListValueChanged(evt);
            }
        });
        jScrollPane1.setViewportView(jList);

        jTextFieldX.setText("0");

        jTextFieldY.setText("0");

        jTextFieldW.setText("800");

        jTextFieldH.setText("600");

        jButton1.setText("1");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jButton2.setText("2");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jButton3.setText("3");
        jButton3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton3ActionPerformed(evt);
            }
        });

        jButton4.setText("4");
        jButton4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton4ActionPerformed(evt);
            }
        });

        jButtonC.setText("Center");
        jButtonC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCActionPerformed(evt);
            }
        });

        jButtonU.setText("Update");
        jButtonU.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonUActionPerformed(evt);
            }
        });

        jButtonR.setText("Reposition");
        jButtonR.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonRActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButtonU, javax.swing.GroupLayout.DEFAULT_SIZE, 284, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 284, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jButton3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton4))
                    .addComponent(jTextFieldX, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jTextFieldY, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jTextFieldW, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jTextFieldH, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jButtonC, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addComponent(jButton1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jButton2))
                    .addComponent(jButtonR, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jTextFieldX, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextFieldY, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextFieldW, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jTextFieldH, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jButtonC)
                        .addGap(11, 11, 11)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton2)
                            .addComponent(jButton1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jButton4)
                            .addComponent(jButton3)))
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 249, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonU)
                    .addComponent(jButtonR))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

	private void jButtonRActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonRActionPerformed
		try {
			Rect rect = rectFromTextFields();
			int i = jList.getSelectedIndex();
			wp.reposition(i, rect.x, rect.y, rect.s, rect.t);
		} catch (Exception e) {
			Logger.getLogger(WPFrame.class.getName()).log(Level.WARNING, "Error parsing?", e);
		}
	}//GEN-LAST:event_jButtonRActionPerformed

	@SuppressWarnings("unchecked")
	private void jButtonUActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonUActionPerformed
		wp.collectWindowHandles();
		wp.filter();
		List<String> list = new LinkedList<String>();
		for (int i = 0; i < wp.getWindowCount(); i++) {
			String s = wp.getWindowText(i);
			list.add(s);
			// TODO: Debug " " output
			Logger.getLogger(WPFrame.class.getName()).log(Level.FINE, "({0})", s);
		}
		jList.setListData(list.toArray());
	}//GEN-LAST:event_jButtonUActionPerformed

	private void jListValueChanged(javax.swing.event.ListSelectionEvent evt) {//GEN-FIRST:event_jListValueChanged
		int i = ((JList)evt.getSource()).getSelectedIndex();
		Rect rect = wp.getWindowRect(i);
		applyRectToTextFields(rect.x, rect.y, rect.s - rect.x, rect.t - rect.y);
	}//GEN-LAST:event_jListValueChanged

	private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
		Rect rect = rectFromTextFields();
		wp.topLeft(rect);
		applyRectToTextFields(rect.x, rect.y, rect.s, rect.t);
	}//GEN-LAST:event_jButton1ActionPerformed

	private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
		Rect rect = rectFromTextFields();
		wp.topRight(rect);
		applyRectToTextFields(rect.x, rect.y, rect.s, rect.t);
	}//GEN-LAST:event_jButton2ActionPerformed

	private void jButton3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton3ActionPerformed
		Rect rect = rectFromTextFields();
		wp.bottomLeft(rect);
		applyRectToTextFields(rect.x, rect.y, rect.s, rect.t);
	}//GEN-LAST:event_jButton3ActionPerformed

	private void jButton4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton4ActionPerformed
		Rect rect = rectFromTextFields();
		wp.bottomRight(rect);
		applyRectToTextFields(rect.x, rect.y, rect.s, rect.t);
	}//GEN-LAST:event_jButton4ActionPerformed

	private void jButtonCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCActionPerformed
		Rect rect = rectFromTextFields();
		rect = wp.calculateCenter(rect);
		applyRectToTextFields(rect.x, rect.y, rect.s, rect.t);
	}//GEN-LAST:event_jButtonCActionPerformed

    /**
    * @param args the command line arguments
    */
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
			@Override
            public void run() {
                new WPFrame().setVisible(true);
            }
        });
    }

	// <editor-fold defaultstate="collapsed" desc="Variables declaration">
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JButton jButton3;
    private javax.swing.JButton jButton4;
    private javax.swing.JButton jButtonC;
    private javax.swing.JButton jButtonR;
    private javax.swing.JButton jButtonU;
    private javax.swing.JList jList;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTextField jTextFieldH;
    private javax.swing.JTextField jTextFieldW;
    private javax.swing.JTextField jTextFieldX;
    private javax.swing.JTextField jTextFieldY;
    // End of variables declaration//GEN-END:variables
	// </editor-fold>

	private void init() {
		wp = new WPFacility();
		Rect rect = new Rect();
		rect.s = getSize().width;
		rect.t = getSize().height;
		rect = wp.calculateCenter(rect);
		this.setLocation(rect.x, rect.y);
		getRootPane().setDefaultButton(jButtonR);
	}

	private Rect rectFromTextFields() {
		Rect rect = new Rect();
		rect.x = Integer.parseInt(jTextFieldX.getText());
		rect.y = Integer.parseInt(jTextFieldY.getText());
		rect.s = Integer.parseInt(jTextFieldW.getText());
		rect.t = Integer.parseInt(jTextFieldH.getText());
		return rect;
	}

	private void applyRectToTextFields(int x, int y, int w, int h) {
		jTextFieldX.setText(Integer.toString(x));
		jTextFieldY.setText(Integer.toString(y));
		jTextFieldW.setText(Integer.toString(w));
		jTextFieldH.setText(Integer.toString(h));
	}
}
