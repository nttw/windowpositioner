package org.bitbucket.nttw.windowpositioner;

import com.sun.jna.Pointer;
import java.awt.Dimension;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.Window;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import org.bitbucket.nttw.windowpositioner.User32.Rect;

public class WPFacility implements User32.WndEnumProc {
	private final List<Pointer> hWnds;
	private final User32 user32;
	private final Window window;

	public WPFacility() {
		hWnds = new LinkedList<Pointer>();
		user32 = User32.INSTANCE;
		window = new JFrame();
	}

	@Override
	public boolean wndEnumProcCallback(Pointer hWnd, int lParam) {
		hWnds.add(hWnd);
		return true;
	}

	public void collectWindowHandles() {
		hWnds.clear();
		user32.EnumWindows(this, 0);
	}

	public Rect getWorkingArea() {
		Rect rect = new Rect();
		Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
		rect.s = dim.width;
		rect.t = dim.height;

		Insets ins = Toolkit.getDefaultToolkit().getScreenInsets(window.getGraphicsConfiguration());
		rect.x -= ins.left;
		rect.y -= ins.top;
		rect.s -= ins.right;
		rect.t -= ins.bottom;

		return rect;
	}

	private Rect getWindowRect(Pointer hWnd) {
		Rect rect = new Rect();
		user32.GetWindowRect(hWnd, rect);
		return rect;
	}

	private String getWindowText(Pointer hWnd) {
		final int capacity = user32.GetWindowTextLengthA(hWnd) + 1;
		byte[] lpString = new byte[capacity];
		user32.GetWindowTextA(hWnd, lpString, capacity);
		return new String(lpString);
	}

	public Rect calculateCenter(Rect rect) {
		Rect result = new Rect();
	    Rect r = getWorkingArea();
		result.x = (r.s - rect.s) / 2;	// s == width
		result.y = (r.t - rect.t) / 2;	// t == height
		return result;
	}

	public void topLeft(Rect rect) {
		rect.x = 0;
		rect.y = 0;
	}

	public void topRight(Rect rect) {
		int w = rect.s;
		Rect r = getWorkingArea();
		rect.x = r.s - w;
		rect.y = 0;
	}

	public void bottomLeft(Rect rect) {
		int h = rect.t;
		Rect r = getWorkingArea();
		rect.x = 0;
		rect.y = r.t - h;
	}

	public void bottomRight(Rect rect) {
		int w = rect.s;
		int h = rect.t;
		Rect r = getWorkingArea();
		rect.x = r.s - w;
		rect.y = r.t - h;
	}

	public void reposition(int i, int x, int y, int w, int h) {
		Pointer hWnd = hWnds.get(i);
		user32.MoveWindow(hWnd, x, y, w, h, true);
	}

	public int getWindowCount() {
		return hWnds.size();
	}

	public String getWindowText(int i) {
		return getWindowText(hWnds.get(i));
	}

	public Rect getWindowRect(int i) {
		return getWindowRect(hWnds.get(i));
	}

	public void removeWindowAt(int i) {
		hWnds.remove(i);
	}

	public void filter() {
		Iterator<Pointer> it = hWnds.iterator();
		while (it.hasNext()) {
			Pointer hwnd = it.next();
			final String s = getWindowText(hwnd);
			final Rect rect = getWindowRect(hwnd);
			final int visible = user32.IsWindowVisible(hwnd);
			if (visible == 0) {
				it.remove();
				Logger.getLogger(WPFacility.class.getName()).log(Level.FINE, "Filtered by visibility: {0}", s);
			} else if (s.isEmpty()) {
				it.remove();
				Logger.getLogger(WPFacility.class.getName()).log(Level.FINE, "Filtered by string: {0}", s);
			} else if (rect.s <= 0 || rect.t <= 0) {
				it.remove();
				Logger.getLogger(WPFacility.class.getName()).log(Level.FINE, "Filtered by size: {0}", s);
			}
		}
	}

	public static void main(String[] args) {
		WPFacility wp = new WPFacility();
		wp.collectWindowHandles();
		for (Pointer hWnd : wp.hWnds) {
			System.out.println(wp.getWindowText(hWnd));
			Rect rect = wp.getWindowRect(hWnd);
			System.out.println(rect.toString());
		}
	}
}
